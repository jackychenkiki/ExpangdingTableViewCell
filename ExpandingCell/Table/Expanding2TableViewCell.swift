//
//  Expanding2TableViewCell.swift
//  ExpandingCell
//
//  Created by JackyChen on 2019/2/22.
//  Copyright © 2019 alexiscreuzot. All rights reserved.
//

import UIKit

class Expanding2TableViewCell: UITableViewCell
{
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var contentStackView: UIStackView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var ageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setContent(_ viewModel: ExpandingViewModel)
    {
        self.titleLabel.text = viewModel.title
        self.nameLabel.text = viewModel.name
        self.ageLabel.text = viewModel.age
        
        self.contentStackView.isHidden = !viewModel.expand
    }

}
