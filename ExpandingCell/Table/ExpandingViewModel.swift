//
//  ExpandingViewModel.swift
//  ExpandingCell
//
//  Created by JackyChen on 2019/2/22.
//  Copyright © 2019 alexiscreuzot. All rights reserved.
//

import Foundation

struct ExpandingViewModel
{
    var title: String!
    
    var name: String!
    
    var age: String!
    
    var expand = false
    
    init(title: String, name: String, age: String)
    {
        self.title = title
        self.name = name
        self.age = age
    }
}
