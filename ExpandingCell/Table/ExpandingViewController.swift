//
//  ExpandingViewController.swift
//  ExpandingCell
//
//  Created by JackyChen on 2019/2/22.
//  Copyright © 2019 alexiscreuzot. All rights reserved.
//

import UIKit

class ExpandingViewController: UIViewController
{
    var expandingViewModels: [ExpandingViewModel]!
 
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        expandingViewModels = [ExpandingViewModel(title: "No1", name: "Jacky", age: "30"),
                               ExpandingViewModel(title: "No2", name: "BensonBensonBensonBensonBensonBensonBensonBensonBensonBensonBensonBensonBenson", age: "10"),
                               ExpandingViewModel(title: "No3", name: "Katy", age: "20"),
                               ExpandingViewModel(title: "No4", name: "Lisa", age: "30"),
                               ExpandingViewModel(title: "No5", name: "KiKi", age: "20")]
        
        tableView.tableFooterView = UIView()
    }
}

extension ExpandingViewController: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return expandingViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: Expanding2TableViewCell.self), for: indexPath) as! Expanding2TableViewCell
        
        cell.setContent(expandingViewModels[indexPath.row])
        
        return cell
    }
}

extension ExpandingViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        expandingViewModels[indexPath.row].expand = !expandingViewModels[indexPath.row].expand
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}
