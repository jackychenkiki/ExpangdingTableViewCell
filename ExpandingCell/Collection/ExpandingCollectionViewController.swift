//
//  ExpandingCollectionViewController.swift
//  ExpandingCell
//
//  Created by JackyChen on 2019/4/26.
//  Copyright © 2019 alexiscreuzot. All rights reserved.
//

import UIKit

class ExpandingCollectionViewController: UIViewController
{
    var expandingViewModels: [ExpandingViewModel]!
    
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        expandingViewModels = [ExpandingViewModel(title: "No1", name: "Jacky", age: "30"),
                               ExpandingViewModel(title: "No2", name: "BensonBensonBensonBensonBensonBensonBensonBensonBensonBensonBensonBensonBenson", age: "10"),
                               ExpandingViewModel(title: "No3", name: "Katy", age: "20"),
                               ExpandingViewModel(title: "No4", name: "LisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisaLisa", age: "30"),
                               ExpandingViewModel(title: "No5", name: "KiKi", age: "20")]
        
        // MARK: - KeyPoint
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width - 20, height: 200)
    }
    
}

extension ExpandingCollectionViewController: UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return expandingViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ExpandingCollectionViewCell.self), for: indexPath) as! ExpandingCollectionViewCell
        
        let expandingViewModel = expandingViewModels[indexPath.row]
        
        cell.setContent(expandingViewModel)
        
        return cell
    }
}

extension ExpandingCollectionViewController: UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        expandingViewModels[indexPath.row].expand = !expandingViewModels[indexPath.row].expand
        
        collectionView.reloadItems(at: [indexPath])
    }
}
