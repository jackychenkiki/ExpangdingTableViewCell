//
//  ExpandingCollectionViewCell.swift
//  ExpandingCell
//
//  Created by JackyChen on 2019/4/26.
//  Copyright © 2019 alexiscreuzot. All rights reserved.
//

import UIKit

class ExpandingCollectionViewCell: UICollectionViewCell
{
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var ageLabel: UILabel!
    
    @IBOutlet var contentStackView: UIStackView!
    
    func setContent(_ viewModel: ExpandingViewModel)
    {
        self.titleLabel.text = viewModel.title
        self.nameLabel.text = viewModel.name
        self.ageLabel.text = viewModel.age
        
        self.contentStackView.isHidden = !viewModel.expand
    }
    
    // MARK: - KeyPoint
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes
    {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame = layoutAttributes.frame
        frame.size.height = ceil(size.height)
        layoutAttributes.frame = frame
        return layoutAttributes
    }
}
